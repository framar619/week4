import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONExample {

    public static String studentToJSON(Student student) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(student);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static Student JSONToStudent(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Student student = null;

        try {
            student = mapper.readValue(s, Student.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return student;
    }

    public static void main(String[] args) {

        Student stu = new Student();
        stu.setName("Frank Cosme");
        stu.setPhone(67081413);
        stu.setCourse("CIT 360");

        String json = JSONExample.studentToJSON(stu);
        System.out.println(json);

        Student stu2 = JSONExample.JSONToStudent(json);
        System.out.println(stu2);
    }

}